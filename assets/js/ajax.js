$(function() {
	var base_url = window.location.origin + "/PPW-C_9";
	loadReview();

	$(document).on("click", "#reviewbtn", function(e) {
		var content = $("#content").val();

		$.ajax({
			type: "POST",
			url: base_url + "/assets/services/review.php",
			data: {instruction: "tambahreview", content: content},
			dataType: 'text',
			success: function(result) {
				loadReview();
			}
		});
	});

	function loadReview() {
		$.ajax({
			type: "GET",
			url: base_url + "/assets/services/review.php",
			data: {instruction: "loadreview"},
			dataType: 'text',
			success: function(result) {
				$("#isireview").html(result);
			}
		});
		$("#content").val("");
	}
});