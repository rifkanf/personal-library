$(function() {
  // reference for smooth scroll: w3school
	$(window).scroll(function() { 
		if ($(this).scrollTop()> 100) { 
			$('#popbooks').fadeIn(2000);
		}
	});

	$(".btn-scroll").on('click', function(event) {
    	if (this.hash !== "") {
      		event.preventDefault();
      		var hash = this.hash;
      		$('html, body').animate({
        		scrollTop: $(hash).offset().top
      		}, 800, function(){
        		window.location.hash = hash;
      		});
    	}
  	});

  	$(window).scroll(function() {
	    $(".slideanim").each(function(){
	      var pos = $(this).offset().top;

	      var winTop = $(window).scrollTop();
	        if (pos < winTop + 600) {
	          $(this).addClass("slide");
	        }
	    });
  	});

    // reference for modal: w3school
    $("#myBtn, #updatebtn").click(function(){
        $("#myModal").modal(options);

        var options = {
          "show" : "true"
        }
    });
      $("#warning").fadeIn(function() {
        setTimeout(function(){
          $("#warning").fadeOut("fast");
        }, 2000);
      });

});