<?php
	session_start();

	$result = "";
	$book_list = array();

	if(isset($_GET["submit_find_book"])) {
		$find_book = $_GET["find_book"];
		$checkbook = false;

		for($i = 0; $i < count($books); $i++) {
			if(stripos($books[$i]->title, $find_book) !== false || stripos($books[$i]->author, $find_book) !== false || stripos($books[$i]->publisher, $find_book) !== false) {
				$checkbook = true;
				array_push($book_list, $books[$i]);
			}
		}

		if(!$checkbook) {
			$result = "No result found.";
		} else {
			$result = "Result(s) for \"" .$find_book ."\"";
		}
	} else {
		$book_list = $books;
		if(count($book_list) == 0) {
			$result = "All books were loaned.";
		}
	}

	if(isset($_GET["updatebookbtn"])) {
		$img_path = $_GET["img_path"];
		$title = $_GET["title"];
		$author = $_GET["author"];
		$publisher = $_GET["publisher"];
		$description = $_GET["description"];
		$quantity = $_GET["quantity"];

		header("Location: " .base_url() .'index.php/admin/update_book?img_path='.$img_path.'&title='.$title.'&author='.$author.'&publisher='.$publisher.'&description='.$description.'&quantity='.$quantity.'');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>BookLand | Books</title>
		<meta charset="UTF-8">
	    <?php include "comp.php"; ?>
	    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>assets/css/book-css.css">
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/book-js.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="background1">
				<?php
					if(isset($_SESSION["username"])) {
						if($_SESSION["role"] == "admin") {
							include "header_admin.php";
						} else {
							include "header_user.php";
						}
					} else {
						include "header.php";
					}
				?>
				<?php include "search_pannel.php"; ?>
			</div>
			<div class="row">
				<div class="container">
					<?php if(count($book_list) == 0) { ?>
						<h3 class="result-judul" style="height: 50vh;">
							<?php echo $result; ?>
						</h3>
					<?php } else { ?>
						<h3 class="result-judul">
							<?php echo $result; ?>
						</h3>
					<?php } ?>
					<?php
	    				for ($i = 0; $i < count($book_list); $i++) { ?>
	      					<div class="row" id="relative">
						      	<div class="col-sm-3 padding">
						      		<a href="<?php echo base_url(); ?>index.php/book/book_detail/<?php echo $book_list[$i]->book_id ?>"><img src='<?= $book_list[$i]->img_path ?>' width='150px' height='220px'></a>
						      	</div>
						      	<div class="col-sm-7">
						      		<div class="padding">
							      		<p><span style='font-weight: bold;'>Title: </span><?= $book_list[$i]->title ?></p>
										<p><span style='font-weight: bold;'>Author: </span><?= $book_list[$i]->author ?></p>
										<p><span style='font-weight: bold;'>Publisher: </span><?= $book_list[$i]->publisher ?></p>
										<p><span style='font-weight: bold;'>Quantity: </span><?= $book_list[$i]->quantity ?></p>
										<?php
											if(isset($_SESSION["username"]) && $_SESSION["role"] == "admin") { ?>
												<div class="col-sm-2">
									        		<a href="<?php echo base_url(); ?>index.php/admin/update/<?php echo $book_list[$i]->book_id ?>"><button class="btn btn-info">Update</button></a>
									        	</div>
									        	<div class="col-sm-2">
									        		<a href="<?php echo base_url(); ?>index.php/admin/delete_book/<?php echo $book_list[$i]->book_id ?>"><button class="btn btn-danger">Delete</button></a>
									        	</div>
									        <?php } elseif($book_list[$i]->quantity > 0) { ?>
								        		<?php
								        			if(isset($_SESSION["username"]) && $_SESSION["role"] == "user") { ?>
								        				<a href='<?php echo base_url(); ?>index.php/book/borrow_book?user_id=<?php echo $_SESSION['user_id'] ?>&book_id=<?php echo $book_list[$i]->book_id ?>'><button name='loanbtn' class='btn btn-default'>Pinjam</button></a>
								        			<?php } ?>
								        	<?php }
								        	else { ?>
		        								<p><span style='font-weight: bold; color: red;'>STOK HABIS</span></p>
		        						<?php }
		        						?>
		        					</div>
		        				</div>
	      					</div>
	  				<?php  }
	  				?>
				</div>	
			</div>
			<?php include "footer.php"; ?>
		</div>
	</body>
</html>