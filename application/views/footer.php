<div class="footer">
    <div class="row" id="footer">
		<div class="col-sm-4">
			<p class="footer-text"> Copyright &copy; 2016 Bookland </p>
		</div>
		<div class="col-sm-4">
			<button onclick="window.location.href='https://www.facebook.com/'" class="fa fa-facebook-official"></button>
			<button onclick="window.location.href='https://id.pinterest.com/'" class="fa fa-pinterest-p"></button>
			<button onclick="window.location.href='https://twitter.com/" class="fa fa fa-twitter"></button>
			<button onclick="window.location.href='https://www.linkedin.com/'" class="fa fa-linkedin-square"></button>
		</div>
		<div class="col-sm-4">
		<p><a class="footer-text" href="#"> About <span class="garis"> | </span> </a><a class="footer-text" href="#"> Privacy <span class="garis"> | </span> </a><a class="footer-text" href="#"> Press <span class="garis"> | </span> </a><a class="footer-text" href="#">Terms of Use <span class="garis"> | </span> </a><a class="footer-text" href="#"> Bookland </a></p>
		</div>
	</div>
</div>