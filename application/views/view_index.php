<?php
	session_start();
	$warning = "";

	if(isset($_POST["submitlogin"])) {
		$username = strtolower($_POST["username"]); 
		$password = $_POST["password"];
		$ceklogin = false;

		foreach($users as $user) {
			if($user->username == $username && $user->password == $password) {
				$ceklogin = true;
				$_SESSION["username"] = $user->username;
	        	$_SESSION["user_id"] = $user->user_id;
	        	$_SESSION["role"] = $user->role;

	        	if($_SESSION["role"] == "admin") {
					header("Location: " .base_url() .'index.php/admin');
				} else {
					header("Location: " .base_url() .'index.php/user');
				}
			}
		}

		if(!$ceklogin) {
			$warning = "Username atau password yang anda masukkan salah.";
		}
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>BookLand</title>
		<meta charset="UTF-8">
	    <?php include "comp.php"; ?>
	    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>assets/css/index-css.css">
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/index-js.js"></script>
	</head>
	<body>
		<p id="warning"><?= $warning; ?></p>
		<div class="container-fluid">
			<div class="background1">
				<nav class="navbar" style="border-bottom-style: solid; border-bottom-color: grey;">
					<div class="col-sm-6">
	    				<ul class="nav navbar-nav">
	      					<li class="active">
	      						<?php
	      							if(!isset($_SESSION["username"])) { ?>
	      								<a href="<?php echo base_url(); ?>" class="white">Home</a>
	      							<?php } else { ?>
	      									<?php
	      										if($_SESSION["role"] == "admin") { ?>
	      											<a href="<?php echo base_url(); ?>index.php/admin" class="white">Home</a>
	      										<?php } else { ?>
	      											<a href="<?php echo base_url(); ?>index.php/user" class="white">Home</a>
	      										<?php }
	      										?>
	      							<?php }
	      							?>
	      					</li>
	      					<li><a href="#promosi" class="white btn-scroll">Trivia</a></li>
	      					<li><a href="#popular-books" class="white btn-scroll">Popular Books</a></li>
	      					<li><a href="<?php echo base_url(); ?>index.php/book" class="white btn-scroll">All Books</a></li>
	    				</ul>
  					</div>
  					<div class="col-sm-6">
  						<?php 
  							if(isset($_SESSION["username"])) { ?>
  								<a href="<?php echo base_url(); ?>index.php/logout"><button type='submit' class='btn btn-warning' style="float: right; margin-top: 6px;">Logout</button></a>
  						<?php
  					 		} else { ?>
  								<button type="button" class="glyphicon glyphicon-user" id="myBtn"><span class="font">Login</span></button>
  								<!-- reference for modal: w3school -->
		  						<div class="modal fade" id="myModal" role="dialog">
		    						<div class="modal-dialog">
							    		<div class="modal-content">
							        		<div class="modal-header" style="padding:35px 50px; background-color: #555555">
							          			<button type="button" class="close" data-dismiss="modal">&times;</button>
							          			<img src="<?php echo base_url(); ?>assets/images/user.png" width="200">
							        		</div>
									        <div class="modal-body" style="padding:40px 50px;">
									        	<form role="form" method="POST" action='<?php echo base_url(); ?>'>
									            	<div class="form-group">
									            		<p>Usernames and passwords do not contain any whitespaces.</p>
									            		<label for="usrname"><span class="glyphicon glyphicon-user"></span> Username</label>
									              		<input type="text" pattern="^[\w]+$" class="form-control" name="username" id="usrname" placeholder="Enter username" required>
									            	</div>
									            	<div class="form-group">
									              		<label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
									              		<input type="password" pattern="^[\S]+$" class="form-control" name="password" id="psw" placeholder="Enter password" required>
									            	</div>
									              	<button type="submit" name="submitlogin" class="btn btn-info btn-block" id="loginbtn"><span class="glyphicon glyphicon-off"></span> Login</button>
									          </form>
									        </div>
							        		<div class="modal-footer">
							          			<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
							        		</div>
							      		</div>
		    						</div>
		 						 </div>
		  					</div>

  							<?php }
  							?>
  	
				</nav>
				<?php include "search_pannel.php"; ?>
			</div>

			<div class="background2" id="popular-books">
				<div class="row">
					<p class="title">Popular Books</p>
					<p class="notes">Experiencing the excitement with upload your amazing files here. <br> Upload your amazing files here and see what will happen.<br> Here are what we provide for you! </p>
				</div>
				<div class="row" id="popbooks">
					<div class="row">
						<?php
							if(count($popular_books) < 3) { ?>
								<p class="nofound"> No books to be shown.</p>
						<?php } else { ?>
							<?php 
			    				for ($i = 0; $i < 3; $i++) { ?>
			      					<div class="col-sm-4">
			        					<a href="<?php echo base_url(); ?>index.php/book/book_detail/<?php echo $popular_books[$i]->book_id ?>"><img src="<?= $popular_books[$i]->img_path; ?>" width='300' height="400"></a>
			        					<p><?= $popular_books[$i]->title;?></p>
			        					<p><?= $popular_books[$i]->author;?></p>
			        					<p>Quantity: <?= $popular_books[$i]->quantity;?></p>
			        					<?php
											if(isset($_SESSION["username"]) && $_SESSION["role"] == "admin") { ?>
												<div class="col-sm-3"></div>
												<div class="col-sm-3">
									        		<a href="<?php echo base_url(); ?>index.php/admin/update/<?php echo $popular_books[$i]->book_id ?>"><button class="btn btn-info">Update</button></a>
									        	</div>
									        	<div class="col-sm-3">
									        		<a href="<?php echo base_url(); ?>index.php/admin/delete_book/<?php echo $popular_books[$i]->book_id ?>"><button class="btn btn-danger">Delete</button></a>
									        	</div>
									        <?php } elseif($popular_books[$i]->quantity > 0) { ?>
								        		<?php
								        			if(isset($_SESSION["username"]) && $_SESSION["role"] == "user") { ?>
								        				<a href='<?php echo base_url(); ?>index.php/book/borrow_book?user_id=<?php echo $_SESSION['user_id'] ?>&book_id=<?php echo $popular_books[$i]->book_id ?>'><button name='loanbtn' class='btn btn-default'>Pinjam</button></a>
								        			<?php } ?>
								        	<?php }
								        	else { ?>
		        								<p><span style='font-weight: bold; color: red;'>STOK HABIS</span></p>
		        						<?php }
		        						?>
			      					</div>
			  				<?php  }
			  				?>
		  				<?php  }
		  				?>
					</div>
				</div>

			<div class="background3" id="promosi">
				<div class="row">
					<!-- reference for carousel plugin: w3school -->
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
					    <ol class="carousel-indicators">
					      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					      <li data-target="#myCarousel" data-slide-to="1"></li>
					      <li data-target="#myCarousel" data-slide-to="2"></li>
					      <li data-target="#myCarousel" data-slide-to="3"></li>
					    </ol>
    					<div class="carousel-inner" role="listbox">
      						<div class="item active">
						        <img src="<?php echo base_url(); ?>assets/images/books1.jpg" alt="books1" height="50">
						        <div class="carousel-caption">
						          	<h3 class="capt">The longest reading aloud marathon by a team lasted 224 hours and was completed by Milton Nan, Silvina Carbone, Carlos Antón, Edit Díaz, Yolanda Baptista and Natalie Dantaz (all Uruguay) at Mac Center Shopping,Paysandú, Uruguay between September 13-22, 2007.</h3>
						        </div>
						    </div>
						    <div class="item">
						        <img src="<?php echo base_url(); ?>assets/images/books2.jpg" alt="books2">
						        <div class="carousel-caption">
						        	<h3 class="capt">A bibliokleptomaniac is someone who steals books. One of the most famous bibliokleptomaniacs is Stephen Blumberg, who stole more than 23,000 rare books from 268 libraries. He had various methods for acquiring his estimated 20 million dollar collection, including climbing through ventilation ducts and elevator shafts.</h3>
						        </div>
						    </div>
						   	<div class="item">
        						<img src="<?php echo base_url(); ?>assets/images/books4.jpg" alt="books4" height="50">
        						<div class="carousel-caption">
          							<h3 class="capt">Research now indicates that the 4- to 6-year-old age range is the sweet spot for teaching reading. Beyond the age of 6 or 7, teaching a child to read is a game of catch up.</h3>
        						</div>
      						</div>
      						<div class="item">
        						<img src="<?php echo base_url(); ?>assets/images/books5.jpg" alt="books5" height="50">
        						<div class="carousel-caption">
          							<h3 class="capt">Adults who read literature on a regular basis are more than two-and-a-half times as likely to do volunteer or charity work, and over one-and-a-half times as likely to participate in sporting activities.</h3>
        						</div>
      						</div>
    					</div>
					    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
					    	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					     	<span class="sr-only">Previous</span>
					    </a>
					    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
					      	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					      	<span class="sr-only">Next</span>
					    </a>
					</div>
				</div>
			</div>

			<div class="background4" id="provide">
				<div class="slideanim">
					<p class="title"> Why Read Books </p>
				</div>
				<div class="row" id="about1">
					<div class="col-sm-4 slideanim">
						<div class="box" id="box1">
							<div class="box-in" id="box-in1">
								<div class="col-sm-10">
									<h3 class="judwhycapt">Improve Understanding</h3>
								</div>
								<div class="col-sm-2">
									<img src="<?php echo base_url(); ?>assets/images/book_icon.png" class="icon">
								</div>								
							</div>
							<p class="whycapt">The more you read, the more you understand one thing: the A to Z of a thing. Let me give an example here: reading allows you learn more about crocodiles and their habits. That you need to be aware of places it usually lurks for, the purpose of staying away from being harmed or bitten. </p>
						</div>				
					</div>
					<div class="col-sm-4 slideanim">
						<div class="box" id="box2">
							<div class="box-in" id="box-in2">
								<div class="col-sm-10">
									<h3 class="judwhycapt">Connecting Your Brain</h3>
								</div>
								<div class="col-sm-2">
									<img src="<?php echo base_url(); ?>assets/images/globe_icon.png" class="icon">
								</div>
							</div>
							<p class="whycapt">When reading, you’re in full silence because reading connects directly to your brain. In silence, you seek for more; in silence, your brain is clear and focuses. Thus, you learn and grow, and therefore you feel and see from the point of view of the author about everything in life. Hence you shape a better self.</p>
						</div>	
					</div>
					<div class="col-sm-4 slideanim">
						<div class="box" id="box3">
							<div class="box-in" id="box-in3">
								<div class="col-sm-10">
									<h3 class="judwhycapt">Boost Creativity</h3>
								</div>
								<div class="col-sm-2">
									<img src="<?php echo base_url(); ?>assets/images/pencil_icon.png" class="icon">
								</div>
							</div>
							<p class="whycapt">Reading exposes you to a world of imagination, showing you nothing is impossible in this world. Books are beyond imagination. It’s like a huge spider web, where you keep linking to more and more to things you knew, and things you just learn, structuring  new solutions and answers.</p>
						</div>
					</div>
				</div>
			</div>
			<?php include "footer.php"; ?>
		</div>
	</body>
</html>

<!-- Why Read Books content source: http://www.inspirationboost.com/8-reasons-why-reading-is-so-important
Trivia content source: http://www.abebooks.com/blog/index.php/tag/book-trivia/ -->