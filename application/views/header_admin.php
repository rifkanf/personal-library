<nav class="navbar navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" id="brand" href="<?php echo base_url(); ?>">BookLand<span class="glyphicon glyphicon-book"></span></a>
    </div>
    <div class="navbar-collapse collapse" id="navbar-collapsible">
      <ul class="nav navbar-nav navbar-right">
        <li><a class="black" href="<?php echo base_url(); ?>index.php/admin">
          <?php if(isset($_SESSION["username"])) { ?>
                        Hi, <?= $_SESSION["username"] ?>!
                      <?php } else { ?>
                            Home
                          <?php }
                          ?>
        </a></li>
        <li><a class="black" href="<?php echo base_url(); ?>index.php/book">All Books</a></li>
        <li><a class="black" href="<?php echo base_url(); ?>index.php/admin/add_book">Add Book</a></li>
        <li>
          <a style="padding-top: 6px; background: transparent; outline: 0;" href="<?php echo base_url(); ?>index.php/logout"><button type='submit' class='btn btn-warning' style="float: right;">Logout</button></a>
        </li>
      </ul>
    </div>
  </div>
</nav>