<?php
	session_start();

	if(!isset($_SESSION["username"])) {
		header("Location: " .base_url());
	} else {
		if($_SESSION["role"] == "admin") {
			header("Location: " .base_url());
		}
	}

	$result = "";
	$book_list = array();
	$checkbook = false;
	for($i = 0; $i < count($books); $i++) {
		if($books[$i]->quantity > 0) {
			array_push($book_list, $books[$i]);
			$checkbook = true;
		}
	}

	if(!$checkbook) {
			$result = "There is no book can be loaned.";
	} else {
		$result = "Books Collection : ";
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>BookLand | User Page</title>
		<meta charset="utf-8">
		<?php include "comp.php"; ?>
	    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>assets/css/user-css.css">
	</head>
	<body>
		<div class="container-fluid">
			<div class="background1">
				<?php include "header_user.php"; ?>
				<?php include "search_pannel.php"; ?>
			</div>
				<div class="row">
					<div class="container">
						<?php if($result == "There is no book can be loaned.") { ?>
							<h3 class="result-judul" style="height: 50vh;">
								<?php echo $result; ?>
							</h3>
						<?php } else { ?>
							<h3 class="result-judul">
								<?php echo $result; ?>
							</h3>
						<?php
		    			for ($i = 0; $i < count($book_list); $i++) { ?>
		      				<div class="row">
						      	<div class="col-sm-3 padding">
						      		<a href="<?php echo base_url(); ?>index.php/book/book_detail/<?php echo $book_list[$i]->book_id ?>"><img src='<?= $book_list[$i]->img_path ?>' width='150px' height='220px'></a>
						      	</div>
						      	<div class="col-sm-9">
						      		<div class="padding">
							      		<p><span style='font-weight: bold;'>Title: </span><?= $book_list[$i]->title ?></p>
										<p><span style='font-weight: bold;'>Author: </span><?= $book_list[$i]->author ?></p>
										<p><span style='font-weight: bold;'>Publisher: </span><?= $book_list[$i]->publisher ?></p>
										<p><span style='font-weight: bold;'>Quantity: </span><?= $book_list[$i]->quantity ?></p>
			        					<?php
								        	if($book_list[$i]->quantity > 0) { ?>
							        		<?php
							        			if(isset($_SESSION["username"])) { ?>
							        				<?php
							        				if($_SESSION["role"] == "admin") { ?>
							        					<div class="col-sm-2">
											        		<a href="<?php echo base_url(); ?>index.php/admin/update/<?php echo $book_list[$i]->book_id ?>"><button class="btn btn-info">Update</button></a>
											        	</div>
											        	<div class="col-sm-2">
											        		<a href="<?php echo base_url(); ?>index.php/admin/delete_book/<?php echo $book_list[$i]->book_id ?>"><button class="btn btn-danger">Delete</button></a>
											        	</div>
							        				<?php } else { ?>
							        						<a href='<?php echo base_url(); ?>index.php/book/borrow_book?user_id=<?php echo $_SESSION['user_id'] ?>&book_id=<?php echo $book_list[$i]->book_id ?>'><button name='loanbtn' class='btn btn-default'>Pinjam</button></a>
							        				<?php }
							        				?>
							        			<?php } ?>
							        	<?php } ?>
							        </div>
							    </div>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
			<?php include "footer.php"; ?>
		</div>
	</body>
</html>