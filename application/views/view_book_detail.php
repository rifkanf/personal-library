<?php
	session_start();
	unset($_SESSION["book_id"]);
	$_SESSION["book_id"] = $book_id;
?>
<!DOCTYPE html>
<html>
	<head>
		<title>BookLand | <?= $book[0]->title ?></title>
		<meta charset="utf-8">
		<?php include "comp.php"; ?>
	    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>assets/css/bookdetail-css.css">
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/book-js.js"></script>
	    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ajax.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="background1">
				<?php
					if(isset($_SESSION["username"])) {
						if($_SESSION["role"] == "admin") {
							include "header_admin.php";
						} else {
							include "header_user.php";
						}
					} else {
						include "header.php";
					}
				?>
			</div>
			<div id="detail">
				<div class="row" id="detailbook" style="padding-top: 70px;">
					<div>
						<p style="font-weight: bold; font-size: 25px; padding-bottom: 30px; text-align: center;"><?= $book[0]->title ?></p>
					</div>
					<div class="col-sm-3">
						<img src="<?= $book[0]->img_path; ?>" width='250px'>
					</div>
					<div class="col-sm-7">
						<p><span style='font-weight: bold;'>Author: </span><?= $book[0]->author ?></p>
						<p><span style='font-weight: bold;'>Publisher: </span><?= $book[0]->publisher ?></p>
						<p><span style='font-weight: bold; text-align: justify;'>Description: </span><?= $book[0]->description ?></p>
						<p><span style='font-weight: bold;'>Quantity: </span><?= $book[0]->quantity ?></p>
				        <?php
							if(isset($_SESSION["username"]) && $_SESSION["role"] == "admin") { ?>
								<div class="col-sm-2">
					        		<a href="<?php echo base_url(); ?>index.php/admin/update/<?php echo $book[0]->book_id ?>"><button class="btn btn-info">Update</button></a>
					        	</div>
					        	<div class="col-sm-2">
					        		<a href="<?php echo base_url(); ?>index.php/admin/delete_book/<?php echo $book[0]->book_id ?>"><button class="btn btn-danger">Delete</button></a>
					        	</div>
					        <?php } elseif($book[0]->quantity > 0) { ?>
				        		<?php
				        			if(isset($_SESSION["username"]) && $_SESSION["role"] == "user") { ?>
				        				<a href='<?php echo base_url(); ?>index.php/book/borrow_book?user_id=<?php echo $_SESSION['user_id'] ?>&book_id=<?php echo $book_id ?>'><button name='loanbtn' class='btn btn-default'>Pinjam</button></a>
				        			<?php } ?>
				        	<?php }
				        	else { ?>
								<p><span style='font-weight: bold; color: red;'>STOK HABIS</span></p>
						<?php }
						?>
					</div>
				</div>
			</div>

		<div class="container" id="review">
			<h2 class="review-judul">Reviews : </h2>
			<?php
				if(isset($_SESSION["username"])) { ?>
					<form method='GET' id="review-form" style="padding-bottom: 30px;">
						<div class='form-group'>
							<textarea class="form-control" rows="5" name="content" placeholder="Write your review here..." id="content"></textarea>
						</div>
						<button type='button' class='btn btn-primary' name='reviewbtn' id="reviewbtn">Submit</button>
					</form>
				<?php }
				?>
				<div id="isireview" style="padding-bottom: 170px;"></div>
		</div>
		<?php include "footer.php"; ?>
	</body>
</html>
