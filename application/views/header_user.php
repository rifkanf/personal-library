<nav class="navbar navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo base_url(); ?>" id="brand">BookLand<span class="glyphicon glyphicon-book"></span></a>
        </div>
        <div class="navbar-collapse collapse" id="navbar-collapsible">
          <ul class="nav navbar-nav navbar-right">
                <li><a class="black" href="<?php echo base_url(); ?>index.php/user">
                  <?php if(isset($_SESSION["username"])) { ?>
                        Hi, <?= $_SESSION["username"] ?>!
                      <?php } else { ?>
                            Home
                          <?php }
                          ?>
                </a></li>
                <li><a class="black" href="<?php echo base_url(); ?>index.php/book">Find Book</a></li>
                <li><a class="black" href="<?php echo base_url(); ?>index.php/user/books_loaned/<?php echo $_SESSION['user_id']; ?>">Books Loaned</a></li>
                   <li>
          <a style="padding-top: 6px; background-color: transparent; outline: 0;" href="<?php echo base_url(); ?>index.php/logout"><button type='submit' class='btn btn-warning' style="float: right;">Logout</button></a>
        </li>
            </ul>
        </div>
      </div>
    </nav>