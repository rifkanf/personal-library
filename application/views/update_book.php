<?php
	session_start();

	if(!isset($_SESSION["username"])) {
		header("Location: " .base_url());
	} else {
		if($_SESSION["role"] == "user") {
			header("Location: " .base_url());
		}
	}

	if(isset($_GET["updatebookbtn"])) {
		$img_path = $_GET["img_path"];
		$title = $_GET["title"];
		$author = $_GET["author"];
		$publisher = $_GET["publisher"];
		$description = $_GET["description"];
		$quantity = $_GET["quantity"];

		header('Location: ' .base_url() .'index.php/admin/update_book?book_id=' .urlencode($book_detail[0]->book_id) .'&img_path=' .urlencode($img_path) .'&title=' .urlencode($title) .'&author=' .urlencode($author) .'&publisher=' .urlencode($publisher) .'&description=' .urlencode($description) .'&quantity=' .urlencode($quantity));
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>BookLand | Update Book</title>
		<meta charset="utf-8">
		<?php include "comp.php"; ?>
		<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>assets/css/addbook-css.css">
	</head>
	<body>
		<div class="container-fluid">
			<div class="background1">
				<?php include "header_admin.php"; ?>
				<?php include "search_pannel.php"; ?>
			</div>
			<div class="row">
				<p class="judul2"> Update Book </p>
				<div class="container" id="form">
					<form method='GET' action='<?php echo base_url(); ?>index.php/admin/update/<?php echo $book_detail[0]->book_id ?>'>
						<div class='form-group'>
							<input type='text' name='img_path' placeholder='Image Path' value="<?= $book_detail[0]->img_path ?>" class='form-control'>
						</div>
						<div class='form-group'>
							<input type='text' name='title' placeholder='Book title' value="<?= $book_detail[0]->title ?>" class='form-control'>
						</div>
						<div class='form-group'>
							<input type='text' name='author' placeholder='Author' value="<?= $book_detail[0]->author ?>" class='form-control'>
						</div>
						<div class='form-group'>
							<input type='text' name='publisher' placeholder='Publisher' value="<?= $book_detail[0]->publisher ?>" class='form-control'>
						</div>
						<div class='form-group'>
							<textarea class="form-control" rows="5" name="description" placeholder="Description"><?= $book_detail[0]->description ?></textarea>
						</div>
						<div class='form-group'>
							<input type='number' name='quantity' placeholder='Quantity' value="<?= $book_detail[0]->quantity ?>" class='form-control'>
						</div>
						<button type='submit' class='btn btn-primary' name='updatebookbtn'>Submit</button>
					</form>
				</div>
			</div>
			<?php include "footer.php"; ?>
		</div>
	</body>
</html>