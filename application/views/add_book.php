<?php
	session_start();

	if(!isset($_SESSION["username"])) {
		header("Location: " .base_url());
	} else {
		if($_SESSION["role"] == "user") {
			header("Location: " .base_url());
		}
	}

	if(isset($_GET["addbookbtn"])) {
		$img_path = $_GET["img_path"];
		$title = $_GET["title"];
		$author = $_GET["author"];
		$publisher = $_GET["publisher"];
		$description = $_GET["description"];
		$quantity = $_GET["quantity"];

		header("Location: " .base_url() .'index.php/book/add_book?img_path='.$img_path.'&title='.$title.'&author='.$author.'&publisher='.$publisher.'&description='.$description.'&quantity='.$quantity.'');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>BookLand | Add Book</title>
		<meta charset="utf-8">
		<?php include "comp.php"; ?>
		<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>assets/css/addbook-css.css">
	</head>
	<body>
		<div class="container-fluid">
			<div class="background1">
				<?php include "header_admin.php"; ?>
				<?php include "search_pannel.php"; ?>
			</div>
			
			<div class="row">
				<p class="judul2"> Add Book </p>
				<div class="container" id="form">
					<form method='GET' action='<?php echo base_url(); ?>index.php/admin/add_book'>
						<div class='form-group'>
							<input type='text' name='img_path' placeholder='Image Path' class='form-control' required>
						</div>
						<div class='form-group'>
							<input type='text' name='title' placeholder='Book title' class='form-control' required>
						</div>
						<div class='form-group'>
							<input type='text' name='author' placeholder='Author' class='form-control' required>
						</div>
						<div class='form-group'>
							<input type='text' name='publisher' placeholder='Publisher' class='form-control' required>
						</div>
						<div class='form-group'>
							<textarea class="form-control" rows="5" name="description" placeholder="Description" required></textarea>
						</div>
						<div class='form-group'>
							<input type='number' name='quantity' placeholder='Quantity' class='form-control' required>
						</div>
						<button type='submit' class='btn btn-primary' name='addbookbtn'>Add Book</button>
					</form>
				</div>
			</div>
			<?php include "footer.php"; ?>
		</div>
	</body>
</html>