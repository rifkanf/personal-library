<div class="row">
	<p id="judul">BookLand</p>
</div>
<div class="row">
	<div id="search-book">
		<form method="GET" action="<?php echo base_url(); ?>index.php/book">
			<input type='text' name='find_book' placeholder='Find book...' class='search'>
			<button type='submit' name='submit_find_book' class="button-search">
  				<span class="glyphicon glyphicon-search"></span>
			</button>
		</form>
	</div>
</div>