<nav class="navbar" role="navigation" style="margin-bottom: -30px;">
	<div class="col-sm-6">
		<ul class="nav navbar-nav">
				<li><a href="<?php echo base_url(); ?>index.php" class="black">
					<?php if(isset($_SESSION["username"])) { ?>
							Hi, <?= $_SESSION["username"] ?>!
						<?php } else { ?>
									Home
								<?php }
								?>
				</a></li>
				<li><a href="<?php echo base_url(); ?>index.php/book" class="black btn-scroll">All Books</a></li>
		</ul>
		</div>
		<div class="col-sm-6">
			<?php 
				if(isset($_SESSION["username"])) { ?>
					<a href="<?php echo base_url(); ?>index.php/logout"><button type='submit' class='btn btn-warning' style="float: right; margin-top: 6px;">Logout</button></a>
			<?php
		 		} else { ?>
					<button type="button" class="glyphicon glyphicon-user" id="myBtn"><span class="font">Login</span></button>
					<!-- reference for modal: w3school -->
					<div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog">
			    		<div class="modal-content">
			        		<div class="modal-header" style="padding:35px 50px; background-color: #555555">
			          			<button type="button" class="close" data-dismiss="modal">&times;</button>
			          			<img src="<?php echo base_url(); ?>assets/images/user.png" width="200">
			        		</div>
					        <div class="modal-body" style="padding:40px 50px;">
					        	<form role="form" method="POST" action='<?php echo base_url(); ?>'>
					            	<div class="form-group">
					            		<p>Usernames and passwords do not contain any whitespaces.</p>
					            		<label for="usrname"><span class="glyphicon glyphicon-user"></span> Username</label>
					              		<input type="text" pattern="^[\w]+$" class="form-control" name="username" id="usrname" placeholder="Enter username" required>
					            	</div>
					            	<div class="form-group">
					              		<label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
					              		<input type="password" pattern="^[\S]+$" class="form-control" name="password" id="psw" placeholder="Enter password" required>
					            	</div>
					              	<button type="submit" name="submitlogin" class="btn btn-info btn-block"><span class="glyphicon glyphicon-off"></span> Login</button>
					          </form>
					        </div>
			        		<div class="modal-footer">
			          			<button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
			        		</div>
			      		</div>
					</div>
					 </div>
				</div>

		<?php }
		?>
</nav>