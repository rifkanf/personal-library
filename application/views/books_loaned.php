<?php
	session_start();

	if(!isset($_SESSION["username"])) {
		header("Location: " .base_url());
	} else {
		if($_SESSION["role"] == "admin") {
			header("Location: " .base_url());
		}
	}
	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>BookLand | Books Loaned</title>
		<meta charset="utf-8">
		<?php include "comp.php"; ?>
	    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>assets/css/user-css.css">
	</head>
	<body>
		<div class="container-fluid">
			<div class="background1">
				<?php include "header_user.php"; ?>
				<?php include "search_pannel.php"; ?>
			</div>
			<div class="container">
				<div class="row">
					<?php if(count($books_loaned) == 0) { ?>
						<p class="result-judul" style="height: 50vh;"> No books are loaned </p>
					<?php } else { ?> 
						<p class="result-judul"> Your books : </p>
						<?php
					    for ($i = 0; $i < count($books_loaned); $i++) { ?>
					      	<div class="row">
						      	<div class="col-sm-3 padding">
						      		<a href="<?php echo base_url(); ?>index.php/book/book_detail/<?php echo $books_loaned[$i]->book_id; ?>"><img src='<?= $books_loaned[$i]->img_path ?>' width='150px'></a>
						      	</div>
						      	<div class="col-sm-7 padding">
						      		<p><span style='font-weight: bold;'>Title: </span><?= $books_loaned[$i]->title ?></p>
									<p><span style='font-weight: bold;'>Author: </span><?= $books_loaned[$i]->author ?></p>
									<p><span style='font-weight: bold;'>Publisher: </span><?= $books_loaned[$i]->publisher ?></p>
									<a href="<?php echo base_url(); ?>index.php/book/return_book?user_id=<?php echo $_SESSION['user_id'] ?>&book_id=<?php echo $books_loaned[$i]->book_id; ?>">
									<button name='returnbtn' class='btn btn-default'>Kembalikan</button></a>
						        </div>
					      </div>
					  <?php  }
					  ?>
					<?php } ?>
				</div>
			</div>
			<?php include "footer.php"; ?>
		</div>
	</body>
</html>