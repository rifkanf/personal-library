<?php
	session_start();

	if(!isset($_SESSION["username"])) {
		header("Location: " .base_url());
	} else {
		if($_SESSION["role"] == "user") {
			header("Location: " .base_url());
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>BookLand | Admin Page</title>
	<meta charset="utf-8">
	<?php include "comp.php"; ?>
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>assets/css/admin-css.css">
</head>
<body>
	<div class="container-fluid">
		<div class="background1">
			<?php include "header_admin.php"; ?>
			<?php include "search_pannel.php"; ?>
			<div class="row">
				<div class="col-sm-6" id="addbook">
					<a class="box" id="box1" href="<?php echo base_url(); ?>index.php/admin/add_book">
						<p class="glyphicon glyphicon-plus icon"></p>
						<p class="text">ADD <br> BOOK</p>
					</a>		
				</div>
				<div class="col-sm-6" id="updatebook">
					<a class="box" id="box2" href="<?php echo base_url(); ?>index.php/book">
						<p class="glyphicon glyphicon-pencil icon"></p>
						<p class="text">UPDATE <br> BOOK</p>
					</a>						
				</div>		
			</div>
		</div>
	</div>
</body>
</html>