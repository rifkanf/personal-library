<?php

class User_model extends CI_Model {
	private $table_name = 'user';

	function  __construct(){
		parent::__construct(); 
	}

	function fetch_user(){
		$query = $this->db->get($this->table_name);
		if($query->num_rows() > 0) return $query->result();
	}

	function fetch_book_loaned($user_id) {
		$this->db->select('*');    
		$this->db->from('book');
		$this->db->join('loan', 'loan.book_id = book.book_id');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get();
		if($query->num_rows() > 0) return $query->result();
	}
}