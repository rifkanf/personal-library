<?php

class Book_model extends CI_Model {
	private $table_name = 'book';

	function  __construct(){
		parent::__construct(); 
	}

	function fetch_book(){
		$query = $this->db->get($this->table_name);
		if($query->num_rows() > 0) return $query->result();
	}

	function fetch_book_detail($id){
		$query = $this->db->get_where($this->table_name, array('book_id' => $id));
		if($query->num_rows() > 0) return $query->result();
	}

	// function fetch_book_review($id) {
	// 	$this->db->select('*');    
	// 	$this->db->from('review');
	// 	$this->db->join('user', 'review.user_id = user.user_id');
	// 	$this->db->where('book_id', $id);
	// 	$query = $this->db->get();
	// 	if($query->num_rows() > 0) return $query->result();
	// }

	function add_new_book($img_path, $title, $author, $publisher, $description, $quantity) {
		$data = array(
			'img_path' => $img_path,
			'title' => $title,
			'author' => $author,
			'publisher' => $publisher,
			'description' => $description,
			'quantity' => $quantity
		);

		$query = $this->db->get_where("book", array("title" => $title));

		$count = $query->num_rows();

		if($count > 0) {
			$book_id = $this->db->get_where("book", array("title" => $title))->row()->book_id;
			$old_quantity = $this->db->get_where("book", array("title" => $title))->row()->quantity;
			$this->update_book_quantity($book_id, $old_quantity+$quantity);

			return $book_id;
		} else {
			$this->db->insert('book', $data);
			return $this->db->get_where($this->table_name, array(
				'img_path' => $img_path,
				'title' => $title,
				'author' => $author,
				'publisher' => $publisher,
				'description' => $description,
				'quantity' => $quantity
			))->row()->book_id;
		}
	}

	function borrow_new_book($user_id, $book_id) {
		$data = array(
			'book_id' => $book_id,
			'user_id' => $user_id
		);

		$this->db->insert("loan", $data);

		$quantity = $this->db->get_where("book", array("book_id" => $book_id))->row()->quantity;
		$this->update_book_quantity($book_id, $quantity-1);
	}

	function update_book_quantity($book_id, $quantity) {
		$this->db->set("quantity", $quantity);
		$this->db->where("book_id", $book_id);
		$this->db->update("book");
	}

	function return_the_book($user_id, $book_id) {
		$loan_id = $this->db->get_where("loan", array(
			"book_id" => $book_id,
			"user_id" => $user_id
		))->row()->loan_id;
		$this->db->where("loan_id", $loan_id);
		$this->db->delete("loan");

		$quantity = $this->db->get_where("book", array("book_id" => $book_id))->row()->quantity;
		$this->update_book_quantity($book_id, $quantity+1);
	}

	// function add_book_review($book_id, $user_id, $date, $content) {
	// 	$data = array(
	// 		"book_id" => $book_id,
	// 		"user_id" => $user_id,
	// 		"date" => $date,
	// 		"content" => $content
	// 	);

	// 	$this->db->insert("review", $data);
	// }

	function delete_a_book($book_id) {
		$this->db->where("book_id", $book_id);
		$this->db->delete("book");
	}

	function update_a_book($book_id, $img_path, $title, $author, $publisher, $description, $quantity) {
		$this->db->set("img_path", $img_path);
		$this->db->set("title", $title);
		$this->db->set("author", $author);
		$this->db->set("publisher", $publisher);
		$this->db->set("description", $description);
		$this->db->set("quantity", $quantity);
		$this->db->where("book_id", $book_id);
		$this->db->update("book");
	}
}