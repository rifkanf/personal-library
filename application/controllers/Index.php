<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Book_model');
		$this->load->model('User_model');
	}

	public function index()
	{	
		$data['title'] = "Personal Library | Expand Your Knowledge";
		$data['popular_books'] = $this->Book_model->fetch_book();
		$data['users'] = $this->User_model->fetch_user();

		$this->load->view('view_index', $data);
	}
}
