<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Book_model');
	}

	public function index()
	{	
		$data['title'] = "Personal Library | Books";
		$data['books'] = $this->Book_model->fetch_book();

		$this->load->view('view_book', $data);
	}

	public function book_detail($id)
	{
		$data['title'] = "Personal Library | Book Detail";
		$data['book'] = $this->Book_model->fetch_book_detail($id);
		// $data['reviews'] = $this->Book_model->fetch_book_review($id);
		$data['book_id'] = $id;

		$this->load->view('view_book_detail', $data);
	}

	public function add_book()
	{
		if($this->input->get()) {
			$img_path = $this->input->get('img_path');
			$title = $this->input->get('title');
			$author = $this->input->get('author');
			$publisher = $this->input->get('publisher');
			$description = $this->input->get('description');
			$quantity = $this->input->get('quantity');

			$data['title'] = "Personal Library | Book Detail";
			$book_id = $this->Book_model->add_new_book($img_path, $title, $author, $publisher, $description, $quantity);

			header("Location: ".base_url() ."index.php/book/book_detail/" .$book_id);
		}
	}

	public function borrow_book() 
	{
		if($this->input->get()) {
			$user_id = $this->input->get('user_id');
			$book_id = $this->input->get('book_id');

			$this->Book_model->borrow_new_book($user_id, $book_id);

			header("Location: " .base_url() ."index.php/user/books_loaned/" .$user_id);
		}

	}

	public function return_book() 
	{
		if($this->input->get()) {
			$user_id = $this->input->get('user_id');
			$book_id = $this->input->get('book_id');

			$this->Book_model->return_the_book($user_id, $book_id);

			header("Location: " .base_url() ."index.php/user/books_loaned/" .$user_id);
		}
	}

	// public function add_review() 
	// {
	// 	if($this->input->get()) {
	// 		$user_id = $this->input->get('user_id');
	// 		$book_id = $this->input->get('book_id');
	// 		$date = $this->input->get('date');
	// 		$content = $this->input->get('content');

	// 		$this->Book_model->add_book_review($book_id, $user_id, $date, $content);
	// 	}
	// }

	public function review()
	{
		$this->load->view('review');
	}
}