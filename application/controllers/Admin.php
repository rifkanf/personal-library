<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Book_model');
	}

	public function index()
	{	
		$data['title'] = "Bookland | Admin Page";

		$this->load->view('view_admin', $data);
	}

	public function books()
	{
		$data['title'] = "Bookland | All Books";
		$data['books'] = $this->Book_model->fetch_book();

		$this->load->view('view_book', $data);
	}

	public function delete_book($id)
	{
		$this->Book_model->delete_a_book($id);

		$this->books();
	}

	public function update($id)
	{
		$data['title'] = "Bookland | Update a Book";
		$data["book_detail"] = $this->Book_model->fetch_book_detail($id);

		$this->load->view("update_book", $data);
	}

	public function update_book()
	{
		if($this->input->get()) {
			$book_id = $this->input->get('book_id');
			$img_path = $this->input->get('img_path');
			$title = $this->input->get('title');
			$author = $this->input->get('author');
			$publisher = $this->input->get('publisher');
			$description = $this->input->get('description');
			$quantity = $this->input->get('quantity');

			$this->Book_model->update_a_book($book_id, $img_path, $title, $author, $publisher, $description, $quantity);

			$data['book'] = $this->Book_model->fetch_book_detail($book_id);
			// $data['reviews'] = $this->Book_model->fetch_book_review($book_id);
			$data['book_id'] = $book_id;

			$this->load->view("view_book_detail", $data);
		}
	}

	public function add_book()
	{
		$data['title'] = "BookLand | Add a Book";
		$this->load->view("add_book", $data);
	}
}
