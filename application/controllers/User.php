<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Book_model');
		$this->load->model('User_model');
	}

	public function index()
	{	
		$data['title'] = "Personal Library | Home";
		$data['books'] = $this->Book_model->fetch_book();
		$this->load->view('view_user', $data);
	}

	public function books_loaned($user_id)
	{
		$data['title'] = "Personal Library | User Page";
		$data['books_loaned'] = $this->User_model->fetch_book_loaned($user_id);

		$this->load->view('books_loaned', $data);
	}
}
